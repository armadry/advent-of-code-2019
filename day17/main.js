console.log("Day 17");

const TURNS = {
  L: {'^': '<', 'v': '>', '>': '^', '<': 'v'},
  R: {'^': '>', 'v': '<', '>': 'v', '<': '^'},
  S: {'^': '^', 'v': 'v', '>': '>', '<': '<'}
}

// part2FindPath();
// part2();

// From an online answer
const compress = str => {
  for (let a = 1; a <= 20; a++) {
    for (let b = 1; b <= 20; b++) {
      for (let c = 1; c <= 20; c++) {
        const matches = {};
        let remaining = str;
        matches.A = remaining.slice(0, a);
        remaining = remaining.replace(new RegExp(matches.A + ',?', 'gu'), '');
        matches.B = remaining.slice(0, b);
        remaining = remaining.replace(new RegExp(matches.B + ',?', 'gu'), '');
        matches.C = remaining.slice(0, c);
        remaining = remaining.replace(new RegExp(matches.C + ',?', 'gu'), '');
        if (!remaining) {
          let compressed = str;
          Object.entries(matches).forEach(
            ([key, value]) => (compressed = compressed.replace(new RegExp(value, 'gu'), key))
          );
          return { compressed, matches };
        }
      }
    }
  }
};

console.log(compress(`L,12,R,4,R,4,R,12,R,4,L,12,R,12,R,4,L,12,R,12,R,4,L,6,L,8,L,8,R,12,R,4,L,6,L,8,L,8,L,12,R,4,R,4,L,12,R,4,R,4,R,12,R,4,L,12,R,12,R,4,L,12,R,12,R,4,L,6,L,8,L,8`));

function part2() {
  /* Full path is;
     L,12,R,4,R,4,R,12,R,4,L,12,R,12,R,4,L,12,R,12,R,4,L,6,L,8,L,8,R,12,R,4,L,6,L,8,L,8,L,12,R,4,R,4,L,12,R,4,R,4,R,12,R,4,L,12,R,12,R,4,L,12,R,12,R,4,L,6,L,8,L,8
   */

  const Main = toCodes("A,B,B,C,C,A,A,B,B,C");

  const A = toCodes("L,12,R,4,R,4");
  const B = toCodes("R,12,R,4,L,12");
  const C = toCodes("R,12,R,4,L,6,L,8,L,8");

  const machine = bufferedIntCode(puzzleInput(), ([, ...data]) => [2, ...data]);
  machine.push(...Main);
  machine.push(...A);
  machine.push(...B);
  machine.push(...C);
  machine.push('n'.charCodeAt(0), 10);

  const score = machine.pop();
  console.log(machine.flush().map(c => String.fromCharCode(c)).join(''));
  console.log('-- << Score >> --');
  console.log(score);
}

function toCodes(list) {
  const codes = Array.from(list).map(s => s.charCodeAt(0));
  codes.push(10);

  return codes;
}

function part2FindPath() {
  const robot = {}
  const grid = makeGrid(robot);

  const fullPath = traverse(grid, robot);
  console.log(JSON.stringify(fullPath));
}

function traverse(grid, robot) {
  const path = [];
  let currentFwdCount = 0;
  let lastTurn;
  let moving = true;

  const pushPath = newTurn => {
    if (lastTurn) {
      path.push(lastTurn, currentFwdCount);
    }
    currentFwdCount = 0;
    lastTurn = newTurn;
    robot.dir = TURNS[newTurn][robot.dir];
  }

  let safety = 3000;

  while(moving) {
    if (!--safety) {
      throw new Error("Safetly valve!");
    }

    let [cell, row, col] = readFwd(grid, robot, 'S');
    if (cell === '#') {
      currentFwdCount++;
      robot.pos = [row, col];
    }
    else {
      [cell] = readFwd(grid, robot, 'R');

      if (cell === '#') {
        pushPath('R');
      }
      else {
        [cell] = readFwd(grid, robot, 'L');

        if (cell === '#') {
          pushPath('L');
        }
        else {
          path.push(lastTurn, currentFwdCount);
          moving = false;
        }
      }
    }
  }

  return path.join(',');
}

function readFwd(grid, {pos: [row, col], dir}, turn ='S') {
  dir = TURNS[turn][dir];

  switch (dir) {
    case '^': {
      if (row === 0) {
        return [];
      }
      return squareAt(grid, row - 1, col);
    }
    case 'v': {
      return squareAt(grid, row + 1, col);
    }
    case '>': {
      return squareAt(grid, row, col + 1);
    }
    case '<': {
      if (col === 0) {
        return [];
      }
      return squareAt(grid, row, col - 1);
    }
  }
}

function squareAt(grid, row, col) {
  return [grid[row][col], row, col];
}

function part1() {
  const grid = makeGrid();
  let intersections = 0;

  grid.forEach((row, r) => {
    row.forEach((cell, c) => {
      if (cell === '#') {
        const a = countAdjacent(grid, r, c, '#');

        if (a > 2) {
          intersections += (r * c);
        }
      }
    });
  });

  console.log(intersections);
}

function countAdjacent(grid, r, c, t) {
  let count = 0;

  if (r > 0 && grid[r - 1][c] === t) {
    count++
  }

  if (grid[r + 1][c] === t) {
    count++
  }

  if (c > 0 && grid[r][c - 1] === t) {
    count++
  }

  if (grid[r][c + 1] === t) {
    count++
  }

  return count;
}

function makeGrid(robot = {}) {
  const output = outputBuffer();

  runIntcode(
    puzzleInput(),
    () => {throw new Error("Unexpected input request")},
    output.receive
  );

  const robotOptions = ['v', '^', '<', '>'];

  const analyzed = output.value.reduce(
    (grid, code) => {
      if (code === 10) {
        grid.push([]);
      }
      else {
        const char = String.fromCharCode(code);
        const row = grid.length - 1;
        const col = grid[row].push(char) - 1;
        if (robotOptions.includes(char)) {
          robot.pos = [row, col];
          robot.dir = char;
        }
      }

      return grid;
    },
    [[]]
  );

  console.log(analyzed.map(row => row.join('')).join("\n"));

  return analyzed;
}

function puzzleInput() {
  return `1,330,331,332,109,3752,1101,1182,0,16,1102,1451,1,24,102,1,0,570,1006,570,36,101,0,571,0,1001,570,-1,570,1001,24,1,24,1106,0,18,1008,571,0,571,1001,16,1,16,1008,16,1451,570,1006,570,14,21101,0,58,0,1105,1,786,1006,332,62,99,21102,333,1,1,21102,73,1,0,1105,1,579,1102,1,0,572,1101,0,0,573,3,574,101,1,573,573,1007,574,65,570,1005,570,151,107,67,574,570,1005,570,151,1001,574,-64,574,1002,574,-1,574,1001,572,1,572,1007,572,11,570,1006,570,165,101,1182,572,127,1002,574,1,0,3,574,101,1,573,573,1008,574,10,570,1005,570,189,1008,574,44,570,1006,570,158,1106,0,81,21102,340,1,1,1106,0,177,21102,1,477,1,1105,1,177,21102,514,1,1,21101,0,176,0,1106,0,579,99,21101,184,0,0,1105,1,579,4,574,104,10,99,1007,573,22,570,1006,570,165,1002,572,1,1182,21101,0,375,1,21101,0,211,0,1106,0,579,21101,1182,11,1,21101,0,222,0,1105,1,979,21101,0,388,1,21101,233,0,0,1106,0,579,21101,1182,22,1,21102,1,244,0,1106,0,979,21102,401,1,1,21101,0,255,0,1106,0,579,21101,1182,33,1,21102,1,266,0,1105,1,979,21101,414,0,1,21102,1,277,0,1106,0,579,3,575,1008,575,89,570,1008,575,121,575,1,575,570,575,3,574,1008,574,10,570,1006,570,291,104,10,21102,1,1182,1,21102,1,313,0,1105,1,622,1005,575,327,1102,1,1,575,21101,327,0,0,1105,1,786,4,438,99,0,1,1,6,77,97,105,110,58,10,33,10,69,120,112,101,99,116,101,100,32,102,117,110,99,116,105,111,110,32,110,97,109,101,32,98,117,116,32,103,111,116,58,32,0,12,70,117,110,99,116,105,111,110,32,65,58,10,12,70,117,110,99,116,105,111,110,32,66,58,10,12,70,117,110,99,116,105,111,110,32,67,58,10,23,67,111,110,116,105,110,117,111,117,115,32,118,105,100,101,111,32,102,101,101,100,63,10,0,37,10,69,120,112,101,99,116,101,100,32,82,44,32,76,44,32,111,114,32,100,105,115,116,97,110,99,101,32,98,117,116,32,103,111,116,58,32,36,10,69,120,112,101,99,116,101,100,32,99,111,109,109,97,32,111,114,32,110,101,119,108,105,110,101,32,98,117,116,32,103,111,116,58,32,43,10,68,101,102,105,110,105,116,105,111,110,115,32,109,97,121,32,98,101,32,97,116,32,109,111,115,116,32,50,48,32,99,104,97,114,97,99,116,101,114,115,33,10,94,62,118,60,0,1,0,-1,-1,0,1,0,0,0,0,0,0,1,58,18,0,109,4,2102,1,-3,587,20101,0,0,-1,22101,1,-3,-3,21102,1,0,-2,2208,-2,-1,570,1005,570,617,2201,-3,-2,609,4,0,21201,-2,1,-2,1105,1,597,109,-4,2105,1,0,109,5,2102,1,-4,630,20101,0,0,-2,22101,1,-4,-4,21101,0,0,-3,2208,-3,-2,570,1005,570,781,2201,-4,-3,653,20101,0,0,-1,1208,-1,-4,570,1005,570,709,1208,-1,-5,570,1005,570,734,1207,-1,0,570,1005,570,759,1206,-1,774,1001,578,562,684,1,0,576,576,1001,578,566,692,1,0,577,577,21102,1,702,0,1106,0,786,21201,-1,-1,-1,1106,0,676,1001,578,1,578,1008,578,4,570,1006,570,724,1001,578,-4,578,21102,731,1,0,1105,1,786,1105,1,774,1001,578,-1,578,1008,578,-1,570,1006,570,749,1001,578,4,578,21101,0,756,0,1106,0,786,1106,0,774,21202,-1,-11,1,22101,1182,1,1,21101,0,774,0,1106,0,622,21201,-3,1,-3,1106,0,640,109,-5,2105,1,0,109,7,1005,575,802,20101,0,576,-6,21001,577,0,-5,1106,0,814,21102,0,1,-1,21102,0,1,-5,21101,0,0,-6,20208,-6,576,-2,208,-5,577,570,22002,570,-2,-2,21202,-5,59,-3,22201,-6,-3,-3,22101,1451,-3,-3,2102,1,-3,843,1005,0,863,21202,-2,42,-4,22101,46,-4,-4,1206,-2,924,21102,1,1,-1,1105,1,924,1205,-2,873,21101,0,35,-4,1105,1,924,1202,-3,1,878,1008,0,1,570,1006,570,916,1001,374,1,374,2101,0,-3,895,1101,0,2,0,2101,0,-3,902,1001,438,0,438,2202,-6,-5,570,1,570,374,570,1,570,438,438,1001,578,558,922,20102,1,0,-4,1006,575,959,204,-4,22101,1,-6,-6,1208,-6,59,570,1006,570,814,104,10,22101,1,-5,-5,1208,-5,39,570,1006,570,810,104,10,1206,-1,974,99,1206,-1,974,1102,1,1,575,21102,973,1,0,1106,0,786,99,109,-7,2105,1,0,109,6,21101,0,0,-4,21102,0,1,-3,203,-2,22101,1,-3,-3,21208,-2,82,-1,1205,-1,1030,21208,-2,76,-1,1205,-1,1037,21207,-2,48,-1,1205,-1,1124,22107,57,-2,-1,1205,-1,1124,21201,-2,-48,-2,1106,0,1041,21101,0,-4,-2,1106,0,1041,21101,-5,0,-2,21201,-4,1,-4,21207,-4,11,-1,1206,-1,1138,2201,-5,-4,1059,2102,1,-2,0,203,-2,22101,1,-3,-3,21207,-2,48,-1,1205,-1,1107,22107,57,-2,-1,1205,-1,1107,21201,-2,-48,-2,2201,-5,-4,1090,20102,10,0,-1,22201,-2,-1,-2,2201,-5,-4,1103,1202,-2,1,0,1106,0,1060,21208,-2,10,-1,1205,-1,1162,21208,-2,44,-1,1206,-1,1131,1105,1,989,21102,439,1,1,1105,1,1150,21101,477,0,1,1105,1,1150,21101,0,514,1,21102,1,1149,0,1106,0,579,99,21102,1,1157,0,1106,0,579,204,-2,104,10,99,21207,-3,22,-1,1206,-1,1138,2102,1,-5,1176,2101,0,-4,0,109,-6,2105,1,0,24,13,46,1,11,1,46,1,11,1,46,1,11,1,46,1,11,13,34,1,23,1,34,1,23,1,34,1,23,1,34,1,23,1,34,1,23,1,34,1,23,1,34,1,23,1,30,5,23,1,30,1,27,1,30,1,25,5,1,1,26,1,25,1,1,1,1,1,1,1,14,5,5,9,17,5,1,1,1,1,14,1,3,1,5,1,1,1,5,1,17,1,1,1,3,1,1,1,14,1,3,1,5,1,1,1,5,1,17,1,1,13,8,1,3,1,5,1,1,1,5,1,17,1,5,1,1,1,6,7,1,1,3,13,1,1,17,1,5,1,1,1,6,1,5,1,1,1,9,1,1,1,3,1,1,1,17,1,5,1,1,1,6,1,5,1,1,1,9,1,1,1,1,5,17,9,6,1,5,1,1,1,9,1,1,1,1,1,1,1,25,1,8,1,5,13,1,5,25,1,8,1,7,1,13,1,27,1,8,1,7,1,13,1,23,5,8,1,7,1,13,1,23,1,12,9,13,1,23,1,34,1,23,1,34,1,23,1,34,1,23,1,34,1,23,1,34,1,23,1,34,13,11,1,46,1,11,1,46,1,11,1,46,1,11,1,46,13,12`;
}
