console.log("Day 22");

// runPart1Test1(...part1Test(0));
// runPart1Test1(...part1Test(1));
// runPart1Test1(...part1Test(2));
// runPart1Test1(...part1Test(3));
// runPart1Test2(...part1Test(0));
// runPart1Test2(...part1Test(1));
// runPart1Test2(...part1Test(2));
// runPart1Test2(...part1Test(3));
// runPart1V2();
runPart2();

function runPart2() {
  const then = performance.now();
  const packLen =       119315717514047;
  const shuffleRepeat = 101741582076661;

  const mul = (a, b) => modMul(a, b, packLen);
  const pow = (a, b) => modPow(a, b, packLen);
  const inv = (a) => modInv(a, packLen);
  const val = (a) => lpr(a, packLen);

  const [a, b] = reduceFns(packLen);

  const A = pow(a, shuffleRepeat);
  const B = mul(
    b,
    mul(
      val(A - 1),
      inv(val(a - 1))
    )
  );

  finalInv = x => mul(val(x - B), inv(A));
  const now = performance.now();

  console.log(finalInv(2020), now - then);
}

function modInv(a, m) {
  return modPow(a, m - 2, m);
}

function optLog(level, ...msgs) {
  if (level <= 0) {
    console.log(...msgs);
  }
}

function modMul(a, b, m) {
  optLog(1, `==== ${a} * ${b} ==================`);
  if (a == 0 || b == 0) {
    return 0;
  }

  let total = 0;

  let currentFactor = a;
  let currentMultiple = b;

  while (currentFactor > 0) {
    optLog(3, {currentFactor});
    if (currentFactor % 2) {
      total = lpr(total + currentMultiple, m);
    }
    currentMultiple = lpr(currentMultiple * 2, m);
    currentFactor = Math.floor(currentFactor / 2);
  }

  optLog(1, `==== ${a} * ${b} = ${total} ==================`);
  return total;
}

function modPow(num, pow, mod) {
  optLog(1, `^^^^ ${num} ^ ${pow} ^^^^^^^^^^^^^^^^^^`);
  let total = 1;
  let currentPower = pow;
  let currentSquare = num;

  while (currentPower > 0) {
    optLog(2, {currentPower});
    if (currentPower % 2) {
      optLog(3, "a");
      total = modMul(total, currentSquare, mod);
    }
    optLog(3, "b");
    currentSquare = modMul(currentSquare, currentSquare, mod);
    currentPower = Math.floor(currentPower / 2);
  }

  optLog(1, `^^^^ ${num} ^ ${pow} = ${total} ^^^^^^^^^^^^^^^^^^`);
  return total;
}

function runPart1V2() {
  const packLen = 10007;
  const [a, b] = reduceFns(packLen);

  const ans = lpr(a * 2019 + b, packLen);

  console.log(lpr(a * modInv(a, packLen), packLen));

  const inv = lpr((ans - b) * modInv(a, packLen), packLen);

  console.log('1867', {a, b, ans, inv});
}

function lpr(a, n) {
  return ((a % n) + n) % n;
}

function reduceFns(packLen) {
  const cmds = parseInstructions(puzzleInput());
  const params = {
    ["deal with increment"](arg) {
      return [arg, 0];
    },
    ["cut"](arg) {
      return [1, -arg];
    },
    ["deal into new stack"]() {
      return [-1, -1];
    }
  };

  let A = 1;
  let B = 0;

  cmds.forEach(({command, arg}) => {
    const [a, b] = params[command](arg);
    A = lpr(a * A, packLen);
    B = lpr((a * B) + b, packLen);
  });

  return [A, B]
}

function runPart1Test2(expectedArray, src) {
  const expected = expectedArray.split(" ")[2];
  const cards = runPart1Instructions(src, 10);
  const {card} = cards.find(e => (e.index == 2));
  console.log("Part 1 Test 2", card == expected, expected, card);
}

function runPart1() {
  const fn = functions(10007);
  const cmds = parseInstructions(puzzleInput());
  let card2019 = 2019;

  cmds.forEach(
    cmd => {
      card2019 = fn[cmd.command](card2019, cmd.arg);
    }
  )

  console.log(card2019);
}

function runPart1Instructions(src, len) {
  const fn = functions(len);
  const cmds = parseInstructions(src);
  const cards = initialise(len);

  cmds.forEach(
    cmd => {
      for (let i = 0; i < len; i++) {
        cards[i].index = fn[cmd.command](cards[i].index, cmd.arg);
      }
    }
  )

  return cards;
}

function runPart1Test1(expected, src) {
  const cards = runPart1Instructions(src, 10);

  const result = cards.sort(
    (...c) => {
      const [a, b] = c.map(({index}) => index);
      return a - b;
    }
  ).map(
    ({card}) => card
  ).join(' ').trim();

  console.log("Part 1 Test 1", result == expected, expected, result);
}

function functions(len) {
  return {
    ["deal with increment"](index, arg) {
      return (index * arg) % len; // safe for part 2 since no given arg takes (arg * index) over MAX_SAFE_INTEGER (max safe arg is 75, max given arg is 74)
    },
    ["cut"](index, arg) {
      return (index + len - arg) % len;
    },
    ["deal into new stack"](index) {
      return len - 1 - index;
    }
  };
}

function initialise(length) {
  return Array.from({ length }, (x, i) => ({card: i, index: i}));
}

function parseInstructions(src) {
  const PARSE_RX = /^([^0-9-]+)([0-9-]*)$/;
  const instructions = src.trim().split("\n").map(
    l => {
      const [,command, arg] = l.trim().match(PARSE_RX);
      return {
        command: command.trim(),
        arg: parseInt(arg, 10)
      };
    }
  );

  return instructions;
}

function part1Test(n) {
  return [
    ["0 3 6 9 2 5 8 1 4 7", `deal with increment 7
      deal into new stack
      deal into new stack`],
    ["3 0 7 4 1 8 5 2 9 6", `cut 6
      deal with increment 7
      deal into new stack`],
    ["6 3 0 7 4 1 8 5 2 9", `deal with increment 7
      deal with increment 9
      cut -2`],
    ["9 2 5 8 1 4 7 0 3 6", `deal into new stack
      cut -2
      deal with increment 7
      cut 8
      cut -4
      deal with increment 7
      cut 3
      deal with increment 9
      deal with increment 3
      cut -1
      `],
  ][n];
}

function puzzleInput() {
  return `deal with increment 73
  cut -6744
  deal into new stack
  cut 9675
  deal with increment 63
  cut -8047
  deal with increment 21
  cut -4726
  deal with increment 39
  cut -537
  deal with increment 39
  cut -6921
  deal with increment 15
  cut 2673
  deal into new stack
  cut 2483
  deal with increment 66
  deal into new stack
  cut 1028
  deal with increment 48
  deal into new stack
  cut -418
  deal with increment 15
  cut 9192
  deal with increment 62
  deal into new stack
  deal with increment 23
  cut 2840
  deal with increment 50
  cut 6222
  deal with increment 20
  deal into new stack
  cut -6855
  deal with increment 50
  cut -1745
  deal with increment 27
  cut 4488
  deal with increment 71
  deal into new stack
  deal with increment 28
  cut -2707
  deal with increment 40
  deal into new stack
  deal with increment 32
  cut 8171
  deal with increment 74
  deal into new stack
  cut -2070
  deal with increment 61
  deal into new stack
  deal with increment 46
  cut 4698
  deal with increment 23
  cut -3480
  deal with increment 30
  cut -6662
  deal with increment 53
  cut -5283
  deal with increment 43
  deal into new stack
  cut -1319
  deal with increment 9
  cut -8990
  deal into new stack
  deal with increment 9
  deal into new stack
  cut -5058
  deal with increment 28
  cut -7975
  deal with increment 57
  cut 2766
  deal with increment 19
  cut 8579
  deal into new stack
  deal with increment 22
  deal into new stack
  cut 9835
  deal with increment 36
  cut -2485
  deal with increment 52
  cut -5818
  deal with increment 9
  cut 5946
  deal with increment 51
  deal into new stack
  cut -5600
  deal with increment 75
  cut -9885
  deal with increment 27
  cut -2942
  deal with increment 68
  cut 3874
  deal with increment 36
  deal into new stack
  deal with increment 20
  cut -2565
  deal with increment 17
  cut -9109
  deal with increment 62
  cut 2175`;
}
