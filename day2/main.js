function stepPart1({data, cursor}) {
  const [op, pIn1, pIn2, pOut] = data.slice(cursor, cursor + 4);
  let running = true;

  switch(op) {
    case 1: {//add
      data[pOut] = data[pIn1] + data[pIn2];
      break;
    }
    case 2: {//mult
      data[pOut] = data[pIn1] * data[pIn2];
      break;
    }
    case 99: {//end
      running = false;
      break;
    }
    default:
      throw new Error(`Unknown opcode ${JSON.stringify({op, pIn1, pIn2, pOut})}`);
  }

  return {running, data, cursor: cursor + 4};
}

function runIntcode(data) {
  let state = {data, cursor: 0, running: true};
  while(state.running) {
    state = stepPart1(state);
  }

  return state.data[0]
}

function runIntcodePart1(data) {
  console.log(runIntcode(data));
}

function runPart2(dataInit) {
  for(let noun = 0; noun < 100; noun++) {
    for(let verb = 0; verb < 100; verb++) {
      const res = runIntcode(dataInit(noun, verb));
      if (res == 19690720) {
        const ans = (100 * noun) + verb;
        console.log({noun, verb, ans});
        return;
      }
    }
  }

  console.log("Not found");
}

// runIntcodePart1(part1TestData(0));
// runIntcodePart1(part1TestData(1));
// runIntcodePart1(part1TestData(2));
// runIntcodePart1(part1TestData(3));
// runIntcodePart1(part1TestData(4));
// runIntcodePart1(part1Data());
runPart2(part1Data);

function parseData(d) {
  return d.split(',').map(d=>parseInt(d, 10));
}

function part1TestData(i) {
  const sets = [
    [3500, '1,9,10,3,2,3,11,0,99,30,40,50'],
    [2, '1,0,0,0,99'],
    [2, '2,3,0,3,99'],
    [2, '2,4,4,5,99,0'],
    [30, '1,1,1,4,99,5,6,0,99']
  ];

  const [expected, data] = sets[i];

  console.log({i, expected})
  return parseData(data);
}

function part1Data(noun = 12, verb = 2) {
  const parsed = parseData('1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,1,6,23,27,1,27,5,31,2,31,10,35,2,35,6,39,1,39,5,43,2,43,9,47,1,47,6,51,1,13,51,55,2,9,55,59,1,59,13,63,1,6,63,67,2,67,10,71,1,9,71,75,2,75,6,79,1,79,5,83,1,83,5,87,2,9,87,91,2,9,91,95,1,95,10,99,1,9,99,103,2,103,6,107,2,9,107,111,1,111,5,115,2,6,115,119,1,5,119,123,1,123,2,127,1,127,9,0,99,2,0,14,0');

  parsed[1] = noun;
  parsed[2] = verb;

  return parsed;
}