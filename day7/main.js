console.log("Part 1 tests");
runPart1(part1TestData(0)[0]);
runPart1(part1TestData(1)[0]);
runPart1(part1TestData(2)[0]);
console.log("Part 1 answer");
runPart1(part1Data());

console.log("Part 2 tests");
runPart2(part2TestData(0)[0]);
runPart2(part2TestData(1)[0]);
console.log("Part 2 answer");
runPart2(part1Data());

function runPart2(srcData) {
  let max = Number.MIN_SAFE_INTEGER;
  let maxSeq;

  phaseSqeuencesPart2().forEach(seq => {
    const thrust = runSequencePart2(srcData, seq);
    if(thrust > max) {
      max = thrust;
      maxSeq = seq.join();
    } 
  });

  console.log({max, from: maxSeq});
}

function runSequencePart2(srcData, seq) {
  let ic1 = startAndPause(srcData, seq[0]);
  let ic2 = startAndPause(srcData, seq[1]);
  let ic3 = startAndPause(srcData, seq[2]);
  let ic4 = startAndPause(srcData, seq[3]);
  let ic5 = startAndPause(srcData, seq[4]);

  let t1, t2, t3, t4, t5;
  t5 = 0;

  let ct = 0;

  while (ic5.running) {
    ct++;
    [ic1, t1] = continueAndPause(ic1, t5);
    [ic2, t2] = continueAndPause(ic2, t1);
    [ic3, t3] = continueAndPause(ic3, t2);
    [ic4, t4] = continueAndPause(ic4, t3);
    [ic5, t5] = continueAndPause(ic5, t4);
  }
  
  return t5;
}

function runSequence(srcData, seq) {
  const t1 = runTwice(srcData, seq[0], 0);
  const t2 = runTwice(srcData, seq[1], t1);
  const t3 = runTwice(srcData, seq[2], t2);
  const t4 = runTwice(srcData, seq[3], t3);
  const t5 = runTwice(srcData, seq[4], t4);

  return t5;
}

function runPart1(srcData) {
  let max = Number.MIN_SAFE_INTEGER;
  let maxSeq;

  allSequences().forEach(seq => {
    const thrust = runSequence(srcData, seq);
    if(thrust > max) {
      max = thrust;
      maxSeq = seq.join();
    } 
  });

  console.log({max, from: maxSeq});
}

function runTwice(srcData, phase, thrustIn) {
  let res;

  runIntcode(
    srcData,
    inputSequence(phase, thrustIn),
    r => {
      res = r;
    }
  );

  return res;
}

function startAndPause(srcData, inp) {
  return runIntcode(
    srcData,
    inputAndPause(inp)
  );
}

function continueAndPause(state, inp) {
  let res = [];

  const newState = continueIntcode(
    state,
    inputAndPause(inp),
    r => {
      res = r;
    }
  );
    
  return [newState, res];
}

function phaseSqeuencesPart2() {
  return allSequences([5,6,7,8,9]);
}

function allSequences(s = [0,1,2,3,4]) {
  var permArr = [],
  usedChars = [];

  function permute(input) {
    var i, ch;
    for (i = 0; i < input.length; i++) {
      ch = input.splice(i, 1)[0];
      usedChars.push(ch);
      if (input.length == 0) {
        permArr.push(usedChars.slice());
      }
      permute(input);
      input.splice(i, 0, ch);
      usedChars.pop();
    }
    return permArr
  };

  return permute(s)
};

function part1TestData(n) {
  const [thrust, sequence, prog] = [
    ['43210', [4,3,2,1,0], `3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0`],
    ['54321', [0,1,2,3,4], `3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0`],
    ['65210', [1,0,4,3,2], `3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0`]
  ][n];

  console.log(`Expect ${thrust} from ${sequence.join(',')}`);

  return [prog, sequence];
}

function part2TestData(n) {
  const [thrust, sequence, prog] = [
    ['139629729', [9,8,7,6,5], `3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5`],
    ['18216', [9,7,8,5,6], `3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10`]
  ][n];

  console.log(`Expect ${thrust} from ${sequence.join(',')}`);

  return [prog, sequence];
}

function part1Data() {
  return `3,8,1001,8,10,8,105,1,0,0,21,42,51,76,93,110,191,272,353,434,99999,3,9,1002,9,2,9,1001,9,3,9,1002,9,3,9,1001,9,2,9,4,9,99,3,9,1002,9,3,9,4,9,99,3,9,1002,9,4,9,101,5,9,9,1002,9,3,9,1001,9,4,9,1002,9,5,9,4,9,99,3,9,1002,9,5,9,101,3,9,9,102,5,9,9,4,9,99,3,9,1002,9,5,9,101,5,9,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99`;
}
