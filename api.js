const http = require("http");
const https = require("https");


//create a server object:
http.createServer(function (req, res) {
    getData('210333').then(
        json => {
            res.write(json || "no data"); //write a response to the client
            res.end(); //end the response        
        },
        e  => {
            res.write(e); //write a response to the client
            res.end(); //end the response        
        }
    );
  }).listen(9003); //the server object listens on port 8080
  

function getData(leaderboardId) {
    return new Promise((resolve, reject) => {
        const url = `https://adventofcode.com/2019/leaderboard/private/view/${leaderboardId}.json`;
        const options = {
          headers: {
            Cookie: 'session=53616c7465645f5f1ad5bd0ca9397b0ca63b177b815c73e9bdc9e93ce79c68707a29d45063c91f4162d19b662d6d3d79'
          }
        };
        const req = https.get(url, options, (res) =>
        {
            let output = '';
            res.setEncoding('utf8');

            res.on('data', function (chunk) {
                output += chunk;
            });

            res.on('end', () => {
                try {
                    let obj = JSON.parse(output);
                    resolve({
                        statusCode: res.statusCode,
                        data: obj
                    });
                }
                catch(err) {
                    reject(err);
                }
            });
        });

        req.on('error', (err) => {
            reject(err);
        });

        req.end();
    });
};