function intCodeStep({data, cursor, relativeBase}, input, output) {
  const opCode = data[cursor];

  const {op, modes} = parseOpCode(opCode);
  let opName;
  let running = true;
  let paused = false;
  let pCount;
  let next = -1;

  function readData(i) {
    return data[i] || 0;
  }

  function writeData(dataIndex, modeIndex, value) {
    switch (modes[modeIndex]) {
      case 2: {// relative mode
        data[dataIndex + relativeBase] = value;
        break;
      }
      case 1: {// immediate mode
        throw new Error(`Can't write in immediate mode ${JSON.stringify({opCode, cursor, relativeBase, modes, modeIndex, mode: modes[modeIndex]})}`);
        break;
      }
      case 0: default: {// position mode
        data[dataIndex]  = value;
        break;
      }
    }
  }

  function pMap(p, i) {
    switch (modes[i]) {
      case 2: {// relative mode
        return readData(p + relativeBase);
      }
      case 1: {// immediate mode
        return p;
      }
      case 0: default: {// position mode
        return readData(p);
      }
    }
  }

  function safeSlice(offsetStart, offsetEnd) {
    const out = [];
    for (let i = offsetStart; i < offsetEnd; i++) {
      out.push(readData(cursor + 1 + i));
    }

    return out;
  }

  function getParams(mapCount = 0) {
    let params = safeSlice(0, mapCount);
    params = params.map(pMap);
    params = params.concat(safeSlice(mapCount, pCount));
    return params;
  }

  switch(op) {
    case 1: {
      opName = 'add';
      pCount = 3;
      const [p1, p2, p3] = getParams(2);
      writeData(p3, 2, p1 + p2);
      break;
    }
    case 2: {
      opName = 'mult';
      pCount = 3;
      const [p1, p2, p3] = getParams(2);
      writeData(p3, 2, p1 * p2);
      break;
    }
    case 3: {
      opName = 'input';
      pCount = 1;
      const [p1] = getParams();
      try {
        writeData(p1, 0, input());
      }
      catch(e) {
        if (e.pause) {
          paused = true;
          next = cursor;
        }
      }
      break;
    }
    case 4: {
      opName = 'output';
      pCount = 1;
      const [p1] = getParams(1);
      output(p1);
      break;
    }
    case 5: {
      opName = 'jump-if-true';
      pCount = 2;
      const [p1, p2] = getParams(2);
      if (p1 !== 0) {
        next = p2;
      }
      break;
    }
    case 6: {
      opName = 'jump-if-false';
      pCount = 2;
      const [p1, p2] = getParams(2);
      if (p1 === 0) {
        next = p2;
      }
      break;
    }
    case 7: {
      opName = 'less-than';
      pCount = 3;
      const [p1, p2, p3] = getParams(2);
      writeData(p3, 2, (p1 < p2) ? 1 : 0);
      break;
    }
    case 8: {
      opName = 'equals';
      pCount = 3;
      const [p1, p2, p3] = getParams(2);
      writeData(p3, 2, (p1 === p2) ? 1 : 0);
      break;
    }
    case 9: {
      opName = 'adjust-relative-base';
      pCount = 1;
      const [p1] = getParams(1);
      relativeBase += p1;

      break;
    }
    case 99: {
      opName = 'end';
      running = false;
      break;
    }
    default:
      throw new Error(`Unknown opcode ${JSON.stringify({opCode, cursor})}`);
  }

  const nextCursor = (next >= 0) ? next : cursor + 1 + pCount;

  return {running, paused, data, cursor: nextCursor, relativeBase};
}

function parseOpCode(code) {
  const op = code % 100;
  let mask = Math.floor(code / 100);
  const modes = [];

  while (mask > 0) {
    modes.push(mask % 10);
    mask = Math.floor(mask / 10);
  }

  return {op, modes};
}

function inputSequence(...seq) {
  let i = 0;
  const {length} = seq;

  return () => {
    const res = seq[i];
    i = (i + 1) % length;
    return res;
  }
}

function inputAndPause(inp) {
  let shouldPause = false;

  return () => {
    if (shouldPause) {
      justPause();
    }
    shouldPause = true;
    return inp;
  }
}

function inputSequenceAndPause(...seq) {
  let i = 0;
  const {length} = seq;

  return () => {
    if (i == length) {
      justPause();
    }
    return seq[i];
  }
}

function justPause() {
  throw { pause: true };
}

function outputBuffer(length) {
  const buffer = [];

  return {
    receive(v) {
      buffer.push(v);
    },
    get value() {
      if (length && buffer.length !== length) {
        throw new Error(`Invalid output ${JSON.stringify(buffer)}`);
      }

      if (length == 1) {
        return buffer[0];
      }

      return buffer;
    }
  }
}

function runIntcode(srcData, input, output) {
  const data = parseIntCodeData(srcData);
  return runParsedIntcode(data, input, output);
}

function runParsedIntcode(data, input, output) {
  let state = cleanInitialState(data);
  return continueIntcode(state, input, output);
}

function cleanInitialState(data) {
  return {
    data,
    cursor: 0,
    running: true,
    relativeBase: 0
  };
}

function isRunning(state) {
  return state.running && !state.paused;
}

function continueIntcode(state, input, output = m => console.log('Output', m)) {
  state.paused = false;
  let safetyNet = 1E7;
  while(isRunning(state) && !!(safetyNet--)) {
    state = intCodeStep(state, input, output);
  }

  if (safetyNet <= 0) {
    console.log("Safety Net Invoked!");
  }

  return state;
}

function parseIntCodeData(prog) {
  return prog.trim().split(',').map(instr => parseInt(instr.trim(), 10));
}

function bufferedIntCode(src, transformParsed = i => i) {
  const data = transformParsed(parseIntCodeData(src));

  return bufferedParsedIntCode(data);
}

function bufferedParsedIntCode(data) {
  const inputBuffer = [];
  const outputBuffer = [];

  function receive(v) {
    outputBuffer.push(v);
  }

  function provide() {
    if (inputBuffer.length) {
      return inputBuffer.shift();
    }
    justPause();
  }

  let state = runParsedIntcode(data, provide, receive);

  return {
    push(...input) {
      inputBuffer.push(...input);
      if(state.paused) {
        state = continueIntcode(state, provide, receive);
      }
    },
    pop() {
      return outputBuffer.pop();
    },
    flush(n = outputBuffer.length) {
      return outputBuffer.splice(0, n);
    },
  };
}

function asciiMachine(src, {
  transform,
  caseSensitiveCommands = false,
  logComments = false,
  logCommands = false,
  scoreCount = 1
} = {}) {
  const machine = bufferedIntCode(src, transform);

  return {
    load(lines) {
      if (!caseSensitiveCommands) {
        lines = lines.toUpperCase();
      }

      lines.trim().split('\n').forEach(
        l => {
          const c = l.indexOf('#');

          if(c >= 0) {
            if (logComments) {
              console.log(l.substr(c));
            }
            l = l.substr(0, c);
          }

          l = l.trim();

          if(l) {
            if (logCommands) {
              console.log(l);
            }
            machine.push(...toAsciiCodes(l));
          }
        }
      );
    },
    output() {
      let score;

      if (scoreCount == 1) {
        score = machine.pop();
      }
      else if (scoreCount > 1) {
        score = machine.flush(scoreCount);
      }
      const text = machine.flush().map(c => String.fromCharCode(c)).join('');
      return {text, score};
    },
    logOutput() {
      const { text, score } = this.output();
      console.log(text.trim());
      if (scoreCount) {
        console.log("Score :", score);
      }
    }
  };
}

function toAsciiCodes(list) {
  const codes = Array.from(list).map(s => s.charCodeAt(0));
  codes.push(10);

  return codes;
}

