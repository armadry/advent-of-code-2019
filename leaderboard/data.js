import {walkObject, DAY_ONE} from './utils.js';

export function allEntries(data) {
  const {members} = data;
  const memberCount = Object.keys(members).length;

  for (let day = 1; day <= 25; day ++) {
      for (let puzzle = 1; puzzle <= 2; puzzle++) {
          const ranking = [];

          walkObject(members, member => {
              const {
                  id,
                  name,
                  completion_day_level = {}
              } = member;

              const {
                  [day]: {
                      [puzzle]: {
                          get_star_ts
                      } = {}
                  } = {}
              } = completion_day_level;

              if (get_star_ts) {
                  ranking.push({id, ts: parseInt(get_star_ts)});
              }
          });

          ranking.sort((...args) => {
              const [a, b] = args.map(r => r.ts);
              return a - b;
          });

          ranking.forEach(({id, ts}, i) => {
              const puzzleData = members[id].completion_day_level[day][puzzle];
              puzzleData.derived = {
                  date: new Date(ts * 1000),
                  tsOffset: ts - DAY_ONE,
                  rank: i + 1,
                  score: memberCount - i
              };
          });
      }

      walkObject(members, member => {
          if (day in member.completion_day_level) {
              const dayData = member.completion_day_level[day]
              const count = Object.keys(dayData).length;

              dayData.derived = {
                  count
              };
          }
      });
  }

  const leaderRank = [];

  walkObject(members, member => {
      let score = 0;

      walkObject(member.completion_day_level, (puzzles = {}, day) => {
          const {
              '1': {
                  derived: {
                      score: score1 = 0
                  } = {}
              } = {},
              '2': {
                  derived: {
                      score: score2 = 0
                  } = {}
              } = {}
          } = puzzles;

          const dayScore = score1 + score2;

          score += dayScore;
      });

      member.derived = {
          score,
      };

      leaderRank.push({member, score});
  });

  leaderRank.sort((...args) => {
      const [a, b] = args.map(r => r.score);
      return b - a;
  });

  leaderRank.forEach(({member}, i) => {
      member.derived.rank = i + 1;
  });

  return leaderRank.map(r => r.member).filter(m => !!m.local_score);
}

/*

Source Data format (TS are Unix seconds)

{
  "event": "2018",
  "members": {
    "{id}": {
      "completion_day_level": {
        "{day}": {
          "1": {
            "get_star_ts": "1544091433"
          },
          "2": {
            "get_star_ts": "1544091678"
          }
        }
      },
      "local_score": 101,
      "global_score": 0,
      "last_star_ts": "1544355371",
      "name": "Tristan Freeman",
      "stars": 18,
      "id": "2693"
    }
  },
  "owner_id": "210333"
}

*/