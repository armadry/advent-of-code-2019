import { DAYS, DAY_LENGTH, DAY_ZERO } from './utils.js';

const dayColors = [
  [212, 144, 210],
  [94, 184, 70],
  [152, 89, 204],
  [168, 180, 57],
  [96, 107, 207],
  [213, 159, 55],
  [206, 87, 189],
  [84, 189, 123],
  [223, 61, 111],
  [85, 197, 174],
  [210, 65, 56],
  [70, 174, 215],
  [225, 114, 52],
  [108, 132, 199],
  [106, 118, 41],
  [196, 68, 138],
  [65, 129, 62],
  [148, 85, 142],
  [156, 179, 105],
  [163, 70, 85],
  [47, 138, 114],
  [226, 126, 134],
  [144, 113, 47],
  [168, 88, 47],
  [219, 161, 110]
];


function ordinal(n) {
  const ords = {
    [1]: '1st',
    [2]: '2nd',
    [3]: '3rd'
  };

  return ords[n] || `${n}th`;
}

function formatTime(date) {
  return `${`${date.getHours()}`.padStart(2, '0')}:${`${date.getMinutes()}`.padStart(2, '0')}`;
}

function formatCompletionTime(data, puzzle, day) {
  const { date } = data[day][puzzle].derived;
  const time = [formatTime(date)];
  const days = Math.floor(((date / 1000) - DAY_ZERO) / DAY_LENGTH) - day;

  if (days == 1) {
    time.push('the next day');
  }
  if (days > 1) {
    time.push(`${days} days later`);
  }

  return time.join(' ');
}

function makeColor(i, a) {
  const [r, g, b] = dayColors[i];
  if (a) {
    return `rgba(${r},${g},${b},${a})`;
  }
  return `rgb(${r},${g},${b})`;
}

export function drawVisualisation(entryData) {
  const canvas = document.querySelector('#viz');
  const ctx = canvas.getContext('2d');

  const regions = [];

  const memberHeight = 77;
  const height = memberHeight * (entryData.length + 1);
  const nameWidth = 230;
  const dayWidth = 64;
  const width = nameWidth + 10 + 25 * dayWidth;

  function makeRegion(x, y, w, h, type, key, data) {
    return {
      type,
      key,
      data,
      box: {
        left: x,
        top: y,
        right: x + w,
        bottom: y + h
      }
    };
  }


  function findRegion(x, y, currentRegion) {
    const found = regions.find(({ data, box: { top, bottom, left, right } }) => {
      return (x > left) && (x < right) && (y > top) && (y < bottom);
    });

    if (found) {
      const { type, key, data } = found;
      const id = `${type}|${key}`;
      if (!currentRegion || (id != currentRegion.id)) {
        return [{ id, type, key, data }, true];
      }
    }

    return [currentRegion, false];
  }

  function manageRegionEvents({ mouseover, click }) {
    let currentRegion = {}, changed;
    canvas.addEventListener('mousemove', ({ offsetX, offsetY }) => {
      [currentRegion, changed] = findRegion(offsetX, offsetY, currentRegion);

      if (changed && mouseover) {
        mouseover(currentRegion, offsetX, offsetY);
      }
    });

    if (click) {
      canvas.addEventListener('click', ({ offsetX, offsetY }) => {
        const [region] = findRegion(offsetX, offsetY);
        click(region, offsetX, offsetY);
      });
    }
  }

  function resetCanvasAndrawAxes(highlight = {}) {
    regions.length = 0;

    canvas.width = width + 1;
    canvas.height = height + 1;

    regions.push(makeRegion(0, 0, nameWidth, memberHeight, 'all-days'));

    ctx.fillStyle = 'white';

    for (let i = 0; i < 25; i++) {
      const x = nameWidth + 10 + i * dayWidth
      ctx.save();

      if (highlight.day && highlight.day != (i + 1)) {
        ctx.fillStyle = makeColor(i, 0.5);
      }
      else {
        ctx.fillStyle = makeColor(i);
      }

      ctx.fillRect(x, 0, dayWidth, memberHeight);
      ctx.restore();

      ctx.save();
      ctx.fillStyle = 'rgba(100, 100, 100, 0.35)';
      ctx.fillRect(x + dayWidth, 0, 1, memberHeight);
      regions.push(makeRegion(x, 0, dayWidth, memberHeight, 'day', i, `${i + 1}`));
      ctx.restore();

      ctx.save();
      ctx.font = '12px monospace';
      ctx.fillText(DAYS[i % 7], x + 5, 25);
      ctx.font = '15px monospace';
      ctx.fillText((i + 1), x + 5, 45);
      ctx.restore();
    }

    ctx.fillRect(nameWidth + 10, 0, 1, height);
    ctx.fillRect(0, memberHeight, width, 1);

    ctx.font = '18px monospace';
    entryData.forEach(({ id, name, derived: { rank, score }, completion_day_level }, i) => {
      ctx.save();

      if (highlight.member && highlight.member != id) {
        ctx.fillStyle = 'rgba(100, 100, 100, 0.5)';
      }

      ctx.fillText(`${rank}.`, 5, (i + 1.55) * memberHeight, 20);
      ctx.fillText(`${name}`, 30, (i + 1.55) * memberHeight, nameWidth - 35);
      ctx.fillText(`${score}`, 30, (i + 1.85) * memberHeight);
      ctx.restore();

      ctx.save();
      ctx.fillStyle = 'rgba(100, 100, 100, 0.35)';
      ctx.fillRect(0, (i + 2) * memberHeight, nameWidth + 10, 1);
      ctx.restore();

      regions.push(makeRegion(0, (i + 1) * memberHeight, nameWidth, memberHeight, 'member', name, entryData[i]));
    });
  }

  function drawDayMarkers() {
    for (let i = 0; i < 25; i++) {
      const x = nameWidth + 10 + (i + 1) * dayWidth;
      ctx.save();
      ctx.fillStyle = 'rgba(100, 100, 100, 0.35)';
      ctx.fillRect(x, memberHeight, 1, height - memberHeight);
      ctx.restore();
    }
  }

  function drawAllDays() {
    resetCanvasAndrawAxes();
    drawDayMarkers();

    entryData.forEach(({ name, completion_day_level }, i) => {
      ctx.save();
      ctx.fillStyle = 'rgba(100, 100, 100, 0.35)';
      ctx.fillRect(nameWidth + 10, (i + 2) * memberHeight, width - nameWidth + 10, 1);
      ctx.restore();

      for (let d = 1; d <= 25; d++) {
        if (completion_day_level[d]) {
          const width = 1;
          if (completion_day_level[d][1]) {
            const start = completion_day_level[d][1].derived.tsOffset * dayWidth / DAY_LENGTH;
            const y = 1 + (3 * d) + (i + 1) * memberHeight;

            ctx.save();
            ctx.fillStyle = makeColor(d - 1, 0.5);
            ctx.fillRect(nameWidth + 10 + (d - 1) * dayWidth, y, start - (d - 1) * dayWidth, 1);
            ctx.restore();

            if (completion_day_level[d][2]) {
              const end = completion_day_level[d][2].derived.tsOffset * dayWidth / DAY_LENGTH;

              ctx.save();
              ctx.fillStyle = makeColor(d - 1);
              ctx.fillRect(start + nameWidth + 10, y - 1, Math.max(end - start, 2), 3);
              ctx.restore();
            }
          }
        }
      }
    });
  }

  function drawDay(day) {
    resetCanvasAndrawAxes({ day });
    ctx.save();
    ctx.fillStyle = makeColor(day - 1);
    const topBarHeight = 15;
    const borderWidth = 2;
    ctx.fillRect(nameWidth + 10 + borderWidth, memberHeight - topBarHeight, 25 * dayWidth - borderWidth, topBarHeight);

    ctx.fillStyle = 'rgba(100, 100, 100, 0.35)';
    ctx.fillRect(nameWidth + 10 + 20 * dayWidth, memberHeight, 1, height - memberHeight);
    ctx.restore();

    entryData.forEach(({ name, completion_day_level }, i) => {
      ctx.save();
      ctx.fillStyle = 'rgba(100, 100, 100, 0.35)';
      ctx.fillRect(nameWidth + 10, (i + 2) * memberHeight, width - nameWidth + 10, 1);
      ctx.restore();

      if (completion_day_level[day]) {
        const width = 1;
        if (completion_day_level[day][1]) {
          const startOffset = completion_day_level[day][1].derived.tsOffset - (day - 1) * DAY_LENGTH;
          const start = Math.min(20 * dayWidth + 10, startOffset * 20 * dayWidth / DAY_LENGTH);
          const y = Math.floor((i + 1.5) * memberHeight);

          ctx.save();
          ctx.fillStyle = makeColor(day - 1, 0.5);
          ctx.fillRect(nameWidth + 12, y, start, 3);

          ctx.font = '18px monospace';
          ctx.fillStyle = 'rgba(200, 200, 200, 0.75)';
          ctx.fillText(ordinal(completion_day_level[day][1].derived.rank), start + nameWidth + 10, y - 9);
          ctx.fillText(formatCompletionTime(completion_day_level, 1, day), start + nameWidth + 50, y - 9);

          ctx.restore();

          if (completion_day_level[day][2]) {
            const endOffset = completion_day_level[day][2].derived.tsOffset - (day - 1) * DAY_LENGTH;
            const end = Math.min(20 * dayWidth + 10, endOffset * 20 * dayWidth / DAY_LENGTH);

            ctx.save();
            ctx.fillStyle = makeColor(day - 1);
            ctx.fillRect(start + nameWidth + 10, y - 2, Math.max(end - start, 7), 7);

            ctx.font = '18px monospace';
            ctx.fillStyle = 'rgba(200, 200, 200, 0.75)';
            ctx.fillText(ordinal(completion_day_level[day][2].derived.rank), end + nameWidth + 10, y + 22);
            ctx.fillText(formatCompletionTime(completion_day_level, 2, day), end + nameWidth + 50, y + 22);

            ctx.restore();
          }
        }
      }
    });
  }

  function drawMember(member) {
    resetCanvasAndrawAxes({ member: member.id });
    drawDayMarkers();

    const i = member.derived.rank - 1;
    const {completion_day_level} = entryData[i];

    for (let day = 1; day <= 25; day++) {
      if (completion_day_level[day]) {
        const width = 1;
        if (completion_day_level[day][1]) {
          const start = completion_day_level[day][1].derived.tsOffset * dayWidth / DAY_LENGTH;
          const y = (20 * day) - 10 + memberHeight;
          const ranks = [
            ordinal(completion_day_level[day][1].derived.rank),
            completion_day_level[day][2] ? ordinal(completion_day_level[day][2].derived.rank) : '-'
          ].join(", ");
          let rankX = start;

          ctx.save();

          ctx.fillStyle = makeColor(day - 1, 0.5);
          ctx.fillRect(nameWidth + 10 + (day - 1) * dayWidth, y, start - (day - 1) * dayWidth, 3);

          ctx.restore();

          if (completion_day_level[day][2]) {
            const end = completion_day_level[day][2].derived.tsOffset * dayWidth / DAY_LENGTH;
            rankX = end;

            ctx.save();
            ctx.fillStyle = makeColor(day - 1);
            ctx.fillRect(start + nameWidth + 10, y - 3, Math.max(end - start, 2), 9);
            ctx.restore();
          }

          ctx.save();

          ctx.font = '12px monospace';
          ctx.fillStyle = 'rgba(200, 200, 200, 0.75)';
          ctx.fillText(ranks, rankX + nameWidth + 15, y + 5);

          ctx.restore();
        }
      }
    }
  }

  const clickHandlers = {
    day: drawDay,
    member: drawMember
  }

  function handleClick(region = {}) {
    const { type, data, id } = region;

    if ((id === currentView) || (type == 'all-days')) {
      currentView = 'all-days';
      drawAllDays();
    }
    else if (clickHandlers[type]) {
      currentView = id;
      clickHandlers[type](data);
    }
  }

  let currentView;
  drawAllDays();
  manageRegionEvents({ click: handleClick }, regions, canvas)
}


