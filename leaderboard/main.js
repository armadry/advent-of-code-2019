import {allEntries} from './data.js';
import {drawVisualisation} from './drawing.js';


export function render(data) {
  const entryData = allEntries(data);
  
  drawVisualisation(entryData);    
}







