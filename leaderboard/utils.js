export const DAY_LENGTH = 24 * 60 * 60;
export const DAY_ZERO = 1575090000; // 5 am 31 Nov
export const DAY_ONE = DAY_ZERO + DAY_LENGTH;
export const DAYS = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

export function walkObject(o, cb) {
  return Object.keys(o).map(k => cb(o[k], k, o));
}