# Advent of Code 2019

Advent of Code 2019 solutions. See day folders for inidividual READMEs.

NGINX full container
docker run --name aoc --mount type=bind,source=C:\Users\hubba\Documents\dev\adventofcode\advent-of-code-2019,target=/usr/share/nginx/html,readonly -p 9001:80 -d nginx 

NODE/EXPRESS Leaderboard
docker run --name aoc-leaders --mount type=bind,source=C:\Users\hubba\Documents\dev\adventofcode\advent-of-code-2019,target=/usr/src/app,readonly -w /usr/src/app -p 9003:9003 -d node:8 node api.js