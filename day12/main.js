console.log("Day 12");
const COORDS = ['x', 'y', 'z'];

part2();

function part2() {
  // 10_000_000 is too low
  const repeats = {};
  const pairs = indexPairs(4);

  COORDS.forEach(c => {
    const moons = parseData(part1Input()); // restart
    // const moons = parseData(part1Test(1)); // restart
    let key = traceKey(moons, c);
    let step = 0;
    const trace = {};

    do {
      // if (step > 2773) {
      //   throw `Too long! ${c}`;
      // }
      // console.log(key)
      trace[key] = step;
      calcStep(moons, pairs);
      key = traceKey(moons, c);
      step++;
    } while (!trace.hasOwnProperty(key));

    const from = trace[key];
    const loop = step - from;

    repeats[c] = {step, from, loop};
  });

  // https://www.calculatorsoup.com/calculators/math/lcm.php
  // https://www.w3resource.com/javascript-exercises/javascript-math-exercise-10.php
  console.log(`${repeats.x.loop} ${repeats.y.loop} ${repeats.z.loop}`);
  // console.log({repeats});
}

function traceKey(moons, c) {
  return moons.map(moon => `${moon.pos[c]},${moon.vel[c]}`).join('|');
}

function part1() {
  const moons = parseData(part1Input());
  const actual = [showMoons(moons)];
  const pairs = indexPairs(moons.length);
  const STEPS = 1000;

  for (let i = 1; i <= STEPS; i++) {
    calcStep(moons, pairs);
  }

  console.log(getTotalEnergy(moons));
}

function part1Test1() {
  const moons = parseData(part1Test(0));
  const expected = part1Example1();
  const actual = [showMoons(moons)];
  const results = [actual[0] == expected[0][1]];
  const pairs = indexPairs(moons.length);

  for (let i = 1; i < expected.length; i++) {
    calcStep(moons, pairs);
    actual.push(showMoons(moons));
    results.push(actual[i] == expected[i][1])
  }

  // expected.forEach((e, i) => {
  //   console.log({eql: results[i], exp: e[1], act: actual[i]});
  // })

  console.log(getTotalEnergy(moons));

}

function part1Test2() {
  const moons = parseData(part1Test(1));
  // const expected = part1Example2();
  const actual = [showMoons(moons)];
  // const results = [actual[0] == expected[0][1]];
  const pairs = indexPairs(moons.length);
  const STEPS = 100;
  // let traceIndex = 1;

  for (let i = 1; i <= STEPS; i++) {
    calcStep(moons, pairs);

    // if (i == expected[traceIndex][0]) {
    //   actual.push(showMoons(moons));
    //   results.push(actual[traceIndex] == expected[traceIndex][1]);
    //   traceIndex++;
    // }
  }

  // expected.forEach((e, i) => {
  //   console.log({eql: results[i], exp: e[1], act: actual[i]});
  // })

  console.log(getTotalEnergy(moons));
}

function getTotalEnergy(moons) {
  return moons.map(m => getMoonEnergy(m)).reduce((t, v) => (t + v));
}

function getMoonEnergy(moon) {
  const e = getEnergy(moon.pos) * getEnergy(moon.vel);
  // console.log('moon', e);
  return e;
}

function getEnergy(p) {
  const e = COORDS.map(c => Math.abs(p[c])).reduce((t, v) => (t + v));
  // console.log('pt', e);
  return e;
}

function calcStep(moons, pairs) {
  // gravity
  pairs.forEach(
    ([a, b]) => {
      COORDS.forEach(
        c => {
          diff = Math.sign(moons[b].pos[c] - moons[a].pos[c]);
          moons[a].vel[c] += diff;
          moons[b].vel[c] -= diff;
        }
      );
    }
  );

  // velocity
  moons.forEach(
    moon => {
      COORDS.forEach(
        c => {
          moon.pos[c] += moon.vel[c];
        }
      );
    }
  );
}


function indexPairs(n) {
  const pairs = [];
  for (let i = 0; i < n - 1; i++) {
    for (let j = i + 1; j < n; j++) {
      pairs.push([i, j]);
    }
  }
  return pairs;
}

function logMoons(m) {
  console.log(showMoons(m));
}

function showMoons(m) {
  return m.map(showMoon).join('\n');
}

function showMoon(m) {
  const {pos, vel} = m;
  return `pos=${showPos(pos)}, vel=${showPos(vel)}`;
}

function showPos({x, y, z}) {
  return `<x=${sp(x)}, y=${sp(y)}, z=${sp(z)}>`
}

function sp(n) {
  return `${n}`;
  // return `${n>=0?' ':''}${n}`;
}

function parseData(d) {
  return d.split('\n').map(parseLine);
}

function parseLine(line) {
  const [x, y, z] = COORDS.map(
    c => parseInt(line.substr(line.indexOf(c) + 2), 10)
  );

  return {pos: {x, y, z}, vel: {x: 0, y: 0, z: 0}};
}

function part1Test(n = 0) {
  return [`<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>`, 
`<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>`
  ][n];
}

function part1Example2() {
  return [

  ];
}

function part1Example1() {
  return [
[0, `pos=<x=-1, y=0, z=2>, vel=<x=0, y=0, z=0>
pos=<x=2, y=-10, z=-7>, vel=<x=0, y=0, z=0>
pos=<x=4, y=-8, z=8>, vel=<x=0, y=0, z=0>
pos=<x=3, y=5, z=-1>, vel=<x=0, y=0, z=0>`],

[1, `pos=<x=2, y=-1, z=1>, vel=<x=3, y=-1, z=-1>
pos=<x=3, y=-7, z=-4>, vel=<x=1, y=3, z=3>
pos=<x=1, y=-7, z=5>, vel=<x=-3, y=1, z=-3>
pos=<x=2, y=2, z=0>, vel=<x=-1, y=-3, z=1>`],

[2, `pos=<x=5, y=-3, z=-1>, vel=<x=3, y=-2, z=-2>
pos=<x=1, y=-2, z=2>, vel=<x=-2, y=5, z=6>
pos=<x=1, y=-4, z=-1>, vel=<x=0, y=3, z=-6>
pos=<x=1, y=-4, z=2>, vel=<x=-1, y=-6, z=2>`],

[3, `pos=<x=5, y=-6, z=-1>, vel=<x=0, y=-3, z=0>
pos=<x=0, y=0, z=6>, vel=<x=-1, y=2, z=4>
pos=<x=2, y=1, z=-5>, vel=<x=1, y=5, z=-4>
pos=<x=1, y=-8, z=2>, vel=<x=0, y=-4, z=0>`],

[4, `pos=<x=2, y=-8, z=0>, vel=<x=-3, y=-2, z=1>
pos=<x=2, y=1, z=7>, vel=<x=2, y=1, z=1>
pos=<x=2, y=3, z=-6>, vel=<x=0, y=2, z=-1>
pos=<x=2, y=-9, z=1>, vel=<x=1, y=-1, z=-1>`],

[5, `pos=<x=-1, y=-9, z=2>, vel=<x=-3, y=-1, z=2>
pos=<x=4, y=1, z=5>, vel=<x=2, y=0, z=-2>
pos=<x=2, y=2, z=-4>, vel=<x=0, y=-1, z=2>
pos=<x=3, y=-7, z=-1>, vel=<x=1, y=2, z=-2>`],

[6, `pos=<x=-1, y=-7, z=3>, vel=<x=0, y=2, z=1>
pos=<x=3, y=0, z=0>, vel=<x=-1, y=-1, z=-5>
pos=<x=3, y=-2, z=1>, vel=<x=1, y=-4, z=5>
pos=<x=3, y=-4, z=-2>, vel=<x=0, y=3, z=-1>`],

[7, `pos=<x=2, y=-2, z=1>, vel=<x=3, y=5, z=-2>
pos=<x=1, y=-4, z=-4>, vel=<x=-2, y=-4, z=-4>
pos=<x=3, y=-7, z=5>, vel=<x=0, y=-5, z=4>
pos=<x=2, y=0, z=0>, vel=<x=-1, y=4, z=2>`],

[8, `pos=<x=5, y=2, z=-2>, vel=<x=3, y=4, z=-3>
pos=<x=2, y=-7, z=-5>, vel=<x=1, y=-3, z=-1>
pos=<x=0, y=-9, z=6>, vel=<x=-3, y=-2, z=1>
pos=<x=1, y=1, z=3>, vel=<x=-1, y=1, z=3>`],

[9, `pos=<x=5, y=3, z=-4>, vel=<x=0, y=1, z=-2>
pos=<x=2, y=-9, z=-3>, vel=<x=0, y=-2, z=2>
pos=<x=0, y=-8, z=4>, vel=<x=0, y=1, z=-2>
pos=<x=1, y=1, z=5>, vel=<x=0, y=0, z=2>`],

[10, `pos=<x=2, y=1, z=-3>, vel=<x=-3, y=-2, z=1>
pos=<x=1, y=-8, z=0>, vel=<x=-1, y=1, z=3>
pos=<x=3, y=-6, z=1>, vel=<x=3, y=2, z=-3>
pos=<x=2, y=0, z=4>, vel=<x=1, y=-1, z=-1>`]
  ];
}


function part1Input() {
  return `<x=-13, y=-13, z=-13>
<x=5, y=-8, z=3>
<x=-6, y=-10, z=-3>
<x=0, y=5, z=-5>`;
}