function testNumber(n) {
  const s = `${n}`;
  let double = false;
  let increase = true;
  for (let i = 0; i < 5; i++) {
    if (s[i] == s[i + 1]) {
      const prevSame = i > 0 && s[i] == s[i - 1];
      const nextSame = i < 5 && s[i + 1] == s[i + 2];
      if(!prevSame && !nextSame) {
        double = true;
      }
    }
    if (s[i + 1] < s[i]) { // single digit!
      increase = false;
    }
  }

  // console.log({n, s, double, increase})
  return (double && increase);
}

console.log(112233, testNumber(112233));
console.log(123444, testNumber(123444));
console.log(111122, testNumber(111122));

let count = 0;
for (n = 272091; n <= 815432; n++) {
  if (testNumber(n)) count++;
}

console.log(count);