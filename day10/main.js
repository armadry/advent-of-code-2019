part1(part1TestData(0));
part1(part1TestData(1));
part1(part1TestData(2));
part1(part1TestData(3));
part1(part1TestData(4));
part1(puzzleInput()[2]);
part2(puzzleInput());

function part2([coords,, inp]) {
  const field = parseField(inp);
  const [c, r] = coords.split(',');
  const station = field.find(({coords: {row, col}}) => {
    return (row == r && col == c);
  });

  let running = true;
  let vaporised = 0;
  let found200th;
  let toVaporise;

  while(running) {
    console.log("looping")
    toVaporise = [];

    walkfield(field, [station], candidate => {
      let blocked = false;
      candidate.diff = undefined;

      walkfield(field, [station, candidate], blocker => {
        if (isBlocked(candidate, blocker, station)) {
          blocked = true;
        }
      });

      
      if(!blocked) {
        toVaporise.push(candidate);
      }
    });

    const {length} = toVaporise;

    if ((vaporised + length) < 200) {
      toVaporise.forEach(v => {
        v.vaporised = true;
      });
    }
    else {
      running = false;
    }
  }

  const index = 200 - vaporised;
  toVaporise.forEach(v => {
    v.stationDiff = diff(v, station);
  })
  const sorted = toVaporise.sort((...vals) => {
    const [a, b] = vals.map(v => v.stationDiff.angle);
    return a - b;
  });

  sorted.forEach(s => {
    console.log(`${s.coords.col}, ${s.coords.row} from station(${station.coords.col}, ${station.coords.row}) is ${s.stationDiff.angle}`);
  })
  
  found200th = sorted[index - 1];

  const ans = (100 * found200th.coords.col) + found200th.coords.row

  console.log({ans, found200th, sorted, angles: sorted.map(a => a.diff.angle), index});
}

function part1(inp) {
  const field = parseField(inp);
  walkfield(field, [], ast => {
    let count = 0;

    walkfield(field, [ast], candidate => {
      let blocked = false;
      candidate.diff = undefined;

      walkfield(field, [ast, candidate], blocker => {
        if (isBlocked(candidate, blocker, ast)) {
          blocked = true;
        }
      });

      if(!blocked) {
        count += 1;
      }
    });

    ast.countNotBlocked = count;
  });

  let max = {countNotBlocked: Number.MIN_SAFE_INTEGER};

  walkfield(field, [], ast => {
    if (max.countNotBlocked < ast.countNotBlocked) {
      max = ast;
    }
  });

  console.log(`${max.countNotBlocked} at ${max.coords.col}, ${max.coords.row}`);
}

function walkfield(field, excluding, fn) {
  field.filter(
    a => (!a.vaporised && !excluding.includes(a))
  ).forEach(fn);
}

function isBlocked(ast, by, from) {
  const diffAst = ast.diff = ast.diff || diff(ast, from);
  const diffBy = diff(by, from);

  const angle = diffAst.angle === diffBy.angle;
  const mag = diffBy.mag < diffAst.mag;
  const blocked = angle && mag;

  return blocked;
}

function getAngle(a, b) {
  return (
    (180 - ((Math.atan(a / b) / Math.PI) * 180)) % 180
  ) + (
    (a < 0) 
      ? 180 
      : (
          (a == 0 && b > 0) 
          ? 180 
          : 0
        )
  )
}


function diff(a, b) {
  const [rowA, rowB] = [a, b].map(x => x.coords.row);
  const [colA, colB] = [a, b].map(x => x.coords.col);
  const row = rowA - rowB;
  const col = colA - colB;
  const angle = getAngle(col, row);
  const mag = Math.abs(row) + Math.abs(col);

  return {row, col, angle, mag};
}

function parseField(f) {
  const field = [];
  f.split('\n').map(
    (l, row) => l.split('').forEach(
      (s, col) => {
        if (s === '#') {
          field.push({
            coords: {row, col}, 
          });
        }
      }
    )
  );
  return field;
}

function part1TestData(n) {
  const [coord, count, field] = [
    ['3,4', 8, `.#..#
.....
#####
....#
...##`],
    ['5,8', 33, `......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####`],
    ['1,2', 35, `#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.`],
    ['6,3', 41, `.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..`],
    ['11,13', 210, `.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##`]
  ][n];

  console.log(`Expect ${count} at ${coord}`);
  return field;
}

function puzzleInput() {
  // part 1 answer 269 at 13, 17
  return ['13,17', 269, `#.#.###.#.#....#..##.#....
.....#..#..#..#.#..#.....#
.##.##.##.##.##..#...#...#
#.#...#.#####...###.#.#.#.
.#####.###.#.#.####.#####.
#.#.#.##.#.##...####.#.##.
##....###..#.#..#..#..###.
..##....#.#...##.#.#...###
#.....#.#######..##.##.#..
#.###.#..###.#.#..##.....#
##.#.#.##.#......#####..##
#..##.#.##..###.##.###..##
#..#.###...#.#...#..#.##.#
.#..#.#....###.#.#..##.#.#
#.##.#####..###...#.###.##
#...##..#..##.##.#.##..###
#.#.###.###.....####.##..#
######....#.##....###.#..#
..##.#.####.....###..##.#.
#..#..#...#.####..######..
#####.##...#.#....#....#.#
.#####.##.#.#####..##.#...
#..##..##.#.##.##.####..##
.##..####..#..####.#######
#.#..#.##.#.######....##..
.#.##.##.####......#.##.##`];
}
