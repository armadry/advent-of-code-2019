function getFuelPart1(mass) {
  return Math.max(0, Math.floor(mass / 3) - 2);
}

function getFuelPart2(mass) {
  let currentMass = mass;
  let currentFuel;
  let totalFuel = 0;

  do {
    currentFuel = getFuelPart1(currentMass);
    totalFuel += currentFuel;
    currentMass = currentFuel;
  }
  while (currentFuel > 0);

  return totalFuel;
}

function runPart1(input) {
  console.log(input.reduce((t, v) => (t + getFuelPart1(v)), 0));
}

function runPart2(input) {
  const addUp = (t, v) => (t + getFuelPart2(v));
  console.log(input.reduce(addUp, 0));
}

runPart2(part1Data());

function part1Test() {
  return [12, 14, 1969, 100756];
}

function part2Test() {
  console.log(2 + 966 + 50346)
  return [14, 1969, 100756];
}

function part1Data() {
  return `85364
97431
135519
119130
137800
85946
146593
141318
103590
138858
92329
94292
132098
144266
72908
112896
87046
133058
141121
74681
83458
107417
121426
66005
106094
96458
113316
142676
79186
55480
147821
116419
70532
105344
116797
126387
139600
136382
121330
123485
134336
141201
131556
91346
117939
58373
129325
102237
60644
96712
126342
98939
66305
111403
143257
58721
55552
139078
74263
125989
90904
91058
92130
53176
81369
100856
110597
111141
129749
123822
75321
80963
102625
70161
107069
117982
86443
95627
147801
149508
101470
81879
133396
82276
144803
67049
127735
121064
122975
69435
139132
141284
70798
117921
108942
85662
75438
122699
116654
126797`.split('\n').map(v => parseInt(v, 10));
}
