console.log("Day 24");

// part1Test1();
// part1Test2();
// runPart1();
// part2Test1();

console.log("Expect 99");
runPart2(part1TestData()[2][0], 10);

runPart2(part1Input(), 200);

function part2Test1() {
  for (let i = 0; i < 25; i++) {
    console.log(i, JSON.stringify(getCheckList(i)));
  }
}

function runPart2(src, count) {
  const grid = parseGrid(src);
  grid[12] = 2; // recursion point

  let layers = new Map();
  layers.set(0, grid);

  for (let i = 0; i < count; i++) {
    // console.log(">>>", i, countBugs(layers));
    layers = nextLayersPart2(layers);
  }

  console.log(">>> End", countBugs(layers));
}

function countBugs(layers) {
  const sum = (a, b) => (a + b);
  return Array.from(layers.entries()).map(
    ([k, e]) => (
      // console.log("-", k),
      // console.log(e, e.reduce(sum)),
      // console.log(vizGrid(e)),
      e.reduce(sum) - 2
    )
  ).reduce(sum);
}

function nextLayersPart2(layers) {
  const next = new Map();

  const {min, max} = Array.from(layers.entries()).reduce(
    ({min, max}, [d]) => ({
      min: Math.min(d, min),
      max: Math.max(d, max)
    }),
    {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER}
  );

  for (let d = min - 1; d <= max + 1; d++) {
    const srcLayer = getLayer(layers, d);
    const nextLayer = nextGridPart1(srcLayer, (g, i) => adjecentCountPart2(layers, d, g, i));

    if(biodiversity(nextLayer) > 0) {
      next.set(d, nextLayer);
    }
  }

  return next;
}

// nextLayer === nextGridPart1(srcLayer, adjecentCountPart2) {

function getCheckList(i) {
  const checkSet = new Set();

  if (i > 4) {
    checkSet.add(i - 5);
  }
  else {
    checkSet.add(-7);
  }

  if (i < 20) {
    checkSet.add(i + 5);
  }
  else {
    checkSet.add(-17);
  }

  if (i % 5 == 0) {
    checkSet.add(-11);
  }
  else {
    checkSet.add(i - 1);
  }

  if (i % 5 == 4) {
    checkSet.add(-13);
  }
  else {
    checkSet.add(i + 1);
  }

  if(checkSet.has(12)) {
    let X = [];
    switch (i) {
      case 7: {
        X = [0,1,2,3,4];
        break;
      }
      case 11: {
        X = [0,5,10,15,20];
        break;
      }
      case 13: {
        X = [4,9,14,19,24];
        break;
      }
      case 17: {
        X = [20,21,22,23,24];
        break;
      }
    }

    X.forEach(x => checkSet.add(x + 25))

    checkSet.delete(12);
  }

  const check = Array.from(checkSet).map(c => ([
    Math.floor(c / 25),
    Math.abs(c % 25)
  ]));

  return check;
}

function adjecentCountPart2(layers, depth, layer, i) {
  const check = getCheckList(i);

  const filtered = check.filter(([d, i]) => getLayer(layers, depth + d)[i] == 1);
  const {length} = filtered;

  return length;
}

function getLayer(layers, depth) {
  if (layers.has(depth)) {
    return layers.get(depth);
  }

  const cleanLayer = Array(25).fill(0);
  cleanLayer[12] = 2;
  return cleanLayer;
}

function runPart1() {
  const store = new Set();
  let grid = parseGrid(part1Input());
  let bio = biodiversity(grid);

  while (!store.has(bio)) {
    store.add(bio);
    grid = nextGridPart1(grid);
    bio = biodiversity(grid);
  }

  console.log(bio);
}

function part1Test1() {
  const [exp, grid, frames] = part1TestData();
  const bio = biodiversity(parseGrid(grid))
  console.log(exp == bio, exp, bio);
}

function part1Test2() {
  const [exp, grid, frames] = part1TestData();
  const l = frames.length - 1;
  for (let i = 0; i < l; i++) {
    const g1 = parseGrid(frames[i]);
    const g2 = parseGrid(frames[i + 1]);
    const g1n = nextGridPart1(g1);
    const b1 = biodiversity(g1n);
    const b2 = biodiversity(g2);
    console.log(i, b1 == b2, b1, b2);
    // console.log(vizGrid(g1));
    // console.log(vizGrid(g1n));
    // console.log(vizGrid(g2));
  }
}

function nextGridPart1(srcGrid, adjacentCount = adjecentCountPart1) {
  return srcGrid.map(
    (g, i, grid) => {
      if (g == 1) { // bug
        if (adjacentCount(grid, i) !== 1) {
          return 0; // dies
        }
      }
      else if (g == 0) { // space
        if ([1, 2].includes(adjacentCount(grid, i))) {
          return 1; // born
        }
      }
      return g;
    }
  )
}

function adjecentCountPart1(grid, i) {
  const check = [];
  if (i > 4) {
    check.push(i - 5);
  }
  if (i < 20) {
    check.push(i + 5);
  }
  if (i % 5 > 0) {
    check.push(i - 1);
  }
  if (i % 5 < 4) {
    check.push(i + 1);
  }

  const filtered = check.filter(c => grid[c] == 1);
  const {length} = filtered;

  return length;
}

function biodiversity(grid) {
  return parseInt(grid.join('').replace(/2/g, '0'), 2);
}

function vizGrid(grid) {
  const chars = ['.', '#', '?'];
  return [...grid].reverse().map(
    (g, i) => `${(i % 5) ? '' : '\n'}${chars[g]}`
  ).join('').trim();
}

function parseGrid(src) {
  return Array.from(
    src.trim()
      .split('\n')
      .map(
        l => l.trim()
      )
      .join('')
  ).map(
    s => ((s == '#') ? 1 : 0)
  ).reverse();
}

function part1TestData() {
  return [2129920,
    `.....
    .....
    .....
    #....
    .#...`,
  [
  `....#
  #..#.
  #..##
  ..#..
  #....`
  ,
  `#..#.
  ####.
  ###.#
  ##.##
  .##..`
  ,
  `#####
  ....#
  ....#
  ...#.
  #.###`
  ,
  `#....
  ####.
  ...##
  #.##.
  .##.#`
  ,
  `####.
  ....#
  ##..#
  .....
  ##...`
  ]];
}

function part1Input() {
  return `#.#.#
  ..#..
  .#.##
  .####
  ###..`;
}