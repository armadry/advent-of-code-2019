console.log("Day 16");

// runPart1(part1TestInput(0));
// runPart1(part1TestInput(1));
// runPart1(part1TestInput(2));
// runPart1(puzzleInput());
runPart2(puzzleInput());

function runPart2(src) {
  let currentPhase = parseInput(src);
  let len = src.length * 5000;
  let offset = parseInt(src.substring(0, 7), 10) - len; // 2ho
  
  for (let i = 0; i < 100; i++) {
    console.log(`phase ${i}`);
    currentPhase = nextPhasePart2(currentPhase, len);
  }

  console.log(currentPhase.slice(offset, offset + 8).join(''));
}

function nextPhasePart2(phase, len) {
  let sum = 0;
  const pLen = phase.length;
  const nextPhase = [];

  for (let i = len - 1; i >= 0; i--) {
    const index = i % pLen;
    sum += phase[index];
    nextPhase[i] = digit(sum);
  }

  return nextPhase;
}

function runPart1(src) {
  let currentPhase = parseInput(src);
  const pattern = patternFactory(src.length);

  for (let i = 0; i < 100; i++) {
    currentPhase = nextPhasePart1(currentPhase, pattern);
  }

  console.log(currentPhase.slice(0, 8).join(''));
}

function nextPhasePart1(phase, pattern) {
  const nextPhase = [];
  for (let i = 0; i < pattern.len; i++) {
    nextPhase[i] = digit(phase.reduce(
      (t, p, j) => {
        const factor = pattern.forIndex(i)[j];
        // console.log({t, i, j, factor, p});
        return t + (factor * p);
      },
      0
    ));
  }
  return nextPhase;
}

function digit(n) {
  return Math.abs(n % 10);
}

function patternFactory(len, clip = true) {
  console.log(len);
  const base = [0, 1, 0, -1];
  const cache = [];

  return {
    get len() {
      return len;
    },
    forIndex(n) {
      if (!cache[n]) {
        // console.log(`make ${n}`);
        const count = n + 1;
        let repeat = [];
        base.forEach(f => {
          repeat = [...repeat, ...Array(count).fill(f)];
        });
        let pattern = [];

        while (pattern.length < len + 2) {
          pattern = [...pattern, ...repeat];
        }
        pattern = pattern.slice(1);
        if (clip) {
          pattern.length = len;
        }
        cache[n] = pattern;
      }
      return cache[n];
    }
  }
}

function parseInput(src) {
  return Array.from(src).map(n => parseInt(n, 10));
}

function part1TestInput(n) {
  const [val, exp] = [
    ['80871224585914546619083218645595', 24176176],
    ['19617804207202209144916044189917', 73745418],
    ['69317163492948606335995924319873', 52432133]
  ][n];

  console.log("expect", exp);
  return val;
}

function puzzleInput() {
  return `59777098810822000809394624382157501556909810502346287077282177428724322323272236375412105805609092414782740710425184516236183547622345203164275191671720865872461284041797089470080366457723972985763645873208418782378044815481530554798953528896905275975178449123276858904407462285078456817038667669183420974001025093760473977009037844415364079145612611938513254763581971458140349825585640285658557835032882311363817855746737733934576748280568150394709654438729776867932430014255649458605325527757230466997043406136400716198065838842158274093116050506775489075879316061475634889155814030818530064869767243196343272137745926937355015378474209347100518533`;
}
